# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 21:02:01 2020

@author: ar54482
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 19:13:16 2020

@author: ar54482
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 14:17:24 2019

@author: ar54482
"""

#Read excel file into python

import pandas as pd
import matplotlib.pyplot as plt
#from scipy.stats import ttest_ind
#import numpy as np
import statistics
import random
import math
import seaborn as sns

dfP = pd.read_csv(r"DataCellShapeFinalPS.csv")
P=dfP['Area']
Pl = dfP["Area"].values

dfN = pd.read_csv(r"DataCellShapeFinalNS.csv")
N=dfN['Area']
Nl = dfN["Area"].values

dfC = pd.read_csv(r"DataCellShapeFinalC.csv")
C=dfC['Area']
Cl = dfC["Area"].values

my_colors=['blue','green','red']


df1=pd.DataFrame({'P Stress':P,'Control':C, 'N Stress':N})
ax=df1.plot(kind="kde",bw_method=0.5,color=my_colors)
#ax.set_xlim(-500,2000)
ax.set_ylim(0,0.00036)

plt.xlabel('Area (in pixels)',fontsize=14)

plt.ylabel(" ")
plt.show()


