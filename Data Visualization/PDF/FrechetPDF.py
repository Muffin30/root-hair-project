# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 21:12:29 2020

@author: ar54482
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 21:09:22 2020

@author: ar54482
"""



#Read excel file into python

import pandas as pd
import matplotlib.pyplot as plt
#from scipy.stats import ttest_ind
#import numpy as np
import statistics
import random
import math
import seaborn as sns

dfP = pd.read_csv(r"DataCellShapeFinalPS.csv")
P=dfP['Frechet']
Pl = dfP["Frechet"].values

dfN = pd.read_csv(r"DataCellShapeFinalNS.csv")
N=dfN['Frechet']
Nl = dfN["Frechet"].values

dfC = pd.read_csv(r"DataCellShapeFinalC.csv")
C=dfC['Frechet']
Cl = dfC["Frechet"].values

my_colors=['blue','green','red']



df1=pd.DataFrame({'P Stress':P,'Control':C, 'N Stress':N})
ax=df1.plot(kind="kde",bw_method=0.5,color=my_colors)
#ax.set_xlim(-50,125)
ax.set_ylim(0,0.016)

plt.xlabel('Frechet (in pixels)',fontsize=14)
##plt.ylabel('Probability Density Function',fontsize=14)
plt.ylabel(" ")
plt.show()


