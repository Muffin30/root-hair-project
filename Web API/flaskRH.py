#RootHair app

import flask
import sqlite3 as sql
from flask import request, jsonify, render_template
from functools import wraps
from flask import request, abort
import sys
import pandas as pd
import json

app = flask.Flask(__name__)
app.config["DEBUG"] = True

#ROUTE0
@app.route('/')
def index():
   return render_template('homefinal.html')




#
##ROUTE2: List recipe specified by id (1 or 2) entered via the URL
@app.route('/Roothair', methods=['GET'])
def api_id():
    # Check if an ID was provided as part of the URL.
    # If ID is provided, assign it to a variable.
    # If no ID is provided, display an error in the browser.
    if 'id' in request.args:
        id = int(request.args.get('id'))
    else:
        return "Error: No id field provided. Please specify an id."
    
    
    try:
        con= sql.connect("TestDB.db")
        print('+=========================+')
        print('|  CONNECTED TO DATABASE  |')
        print('+=========================+')
    except Exception as e:
        sys.exit('error',e)
        
    cur=con.cursor()
    cur.execute("""
            SELECT * FROM ROOTHAIR  
            WHERE ID==id
            """)

    df=pd.DataFrame(cur.fetchall(),columns=['ID','Area','Perimeter','Hausdorf','Frechet','Stress'])
    df=df.to_dict(df)
    j=json.dumps(df)
    return (j)
    con.close()


      
if __name__ == '__main__':
   app.run(host="localhost", port=5000, debug=True)