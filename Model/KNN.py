# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 13:20:14 2020

@author: ar54482
"""

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import accuracy_score

import matplotlib.pyplot as plt
import pandas as pd

P=pd.read_csv(r"DataCellShapeFinalNS.csv")
N=pd.read_csv(r"DataCellShapeFinalPS.csv")
C=pd.read_csv(r"DataCellShapeFinalC.csv")

frames=[P,N,C]
df=pd.concat(frames)

X=df.drop(columns=['Stress'])
y=df.Stress
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = 0.33, random_state = 10)

scaler=MinMaxScaler()
X_train=scaler.fit_transform(X_train)
X_test=scaler.fit_transform(X_test)


clf=GridSearchCV(KNeighborsClassifier(),param_grid={"n_neighbors":[1,2,3,4,5,6,7,8],"weights":["uniform","distance"]},cv=5)
fit=clf.fit(X_train,y_train)
print("KNeighborsClassifier")
print(fit.score(X_test,y_test))
fit.best_params_

