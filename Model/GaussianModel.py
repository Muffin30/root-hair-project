# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 19:22:23 2020

@author: ar54482
"""

from sklearn.naive_bayes import GaussianNB
from sklearn.cross_validation import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import BernoulliNB
import matplotlib.pyplot as plt
import pandas as pd

P=pd.read_csv(r"DataCellShapeFinalNS.csv")
N=pd.read_csv(r"DataCellShapeFinalPS.csv")
C=pd.read_csv(r"DataCellShapeFinalC.csv")

frames=[P,N,C]
df=pd.concat(frames)

X=df.drop(columns=['Stress'])
y=df.Stress
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = 0.33, random_state = 10)

#GaussianNB
gnb=GaussianNB()
gnb_fit=gnb.fit(X_train,y_train)
#print(gnb_fit.score(X_train,y_train))
y_predg = gnb.predict(X_test)

Ag=accuracy_score(y_test, y_predg)
print("Gaussian NB")
print(Ag)
#95%

#MultinomialNB
gnb=MultinomialNB()
gnb_fit=gnb.fit(X_train,y_train)
y_predm = gnb.predict(X_test)

Am=accuracy_score(y_test, y_predm)
print("Multinomial NB")
print(Am)
#76%

#BernoulliNB
gnb=BernoulliNB()
gnb_fit=gnb.fit(X_train,y_train)
y_predb = gnb.predict(X_test)

Ab=accuracy_score(y_test, y_predb)
print("Bernoulli NB")
print(Ab)
#51%